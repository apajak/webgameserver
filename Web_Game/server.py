
from ast import Str
from glob import glob
from random import *
import string
from tokenize import Name
from types import SimpleNamespace
from unicodedata import name
from urllib.request import Request
from wsgiref.util import request_uri
from xmlrpc.client import Boolean
from zipapp import create_archive
from numpy import append, array, true_divide
from flask import Flask, render_template, request, url_for, redirect
import mariadb
import sys
import json


app = Flask(__name__)
cursor = None
webGameDB = None
UserList = []
DBUserList =[]
GameList = []
GameidList = []

def getConnection():
    global cursor
    global webGameDB
    # Connect to MariaDB Platform
    try:
        webGameDB = mariadb.connect(
            user="server1",
            password="server1",
            host="127.0.0.1",
            port=3306,
            database="webGameDB"

        )
    except mariadb.Error as e:
        print(f"Error connecting to MariaDB Platform: {e}")
        sys.exit(1)

    # Get Cursor
    cursor = webGameDB.cursor()
def addUserToDB(CurrentUser):
    getConnection()
    jso = json.dumps(CurrentUser.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "INSERT INTO Users (userData) VALUES ('{}');".format(jso)
        )
        print("Successfully added user to database")
    except mariadb.Error as e:
        print(f"Error adding user to database: {e}")
    webGameDB.commit() 
    CurrentUser.id = cursor.lastrowid
    UpdateDBUser(CurrentUser)

def addGameToDB(CurrentGame):
    getConnection()
    jso = json.dumps(CurrentGame.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "INSERT INTO Games (gameData) VALUES ('{}');".format(jso)
        )
        print("Successfully added game to database")
    except mariadb.Error as e:
        print(f"Error adding game to database: {e}")
    webGameDB.commit() 
    CurrentGame.DBid = cursor.lastrowid
    UpdateDBGame(CurrentGame)

def UpdateDBUser(CurrentUser):
    getConnection()
    jso = json.dumps(CurrentUser.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "update Users set userData = '{}' where id = {};".format(jso,CurrentUser.id)
        )
        print("Successfully update entry to database")
    except mariadb.Error as e:
        print(f"Error update entry to database: {e}")
    webGameDB.commit() 
    CurrentUser.id = cursor.lastrowid

    webGameDB.close()

def UpdateDBGame(CurrentGame):
    getConnection()
    jso = json.dumps(CurrentGame.__dict__)
    global cursor
    global webGameDB
    try:
        cursor.execute(
            "update Games set gameData = '{}' where id = {};".format(jso,CurrentGame.DBid)
        )
        print("Successfully update Game to database")
    except mariadb.Error as e:
        print(f"Error update Game to database: {e}")
    webGameDB.commit() 
    CurrentGame.DBid = cursor.lastrowid
    webGameDB.close()

def getUserList():
    getConnection()
    global cursor
    global webGameDB
    global UserList
    global DBUserList
    UserList = []
    try:
        cursor.execute(
            "SELECT userData FROM Users;"
        )
        for userData in cursor:
            jsonData = ','.join(userData)
            j = json.loads(jsonData)
            user = UserDB(**j)
            UserList.append(user)
            print("Successfully load database")
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")
       
    webGameDB.commit() 
    webGameDB.close()

def getGameList():
    getConnection()
    global cursor
    global webGameDB
    global GameList
    GameList = []
    try:
        cursor.execute(
            "SELECT gameData FROM Games;"
        )
        for gameData in cursor:
            jsonData = ','.join(gameData)
            j = json.loads(jsonData)
            game = GameDB(**j)
            GameList.append(game)
            print("Successfully load Game database")
    except mariadb.Error as e:
        print(f"Error load entry to Game database: {e}")
       
    webGameDB.commit() 
    webGameDB.close()

def rmUserToDB(user):
    global cursor
    global webGameDB
    getConnection()
    id = user.id
    try:
        cursor.execute(
            "delete from Users where id = {};".format(id)
        )
        print("Successfully rm entry to database")
    except mariadb.Error as e:
        print(f"Error adding entry to database: {e}")
    webGameDB.commit() 
    webGameDB.close()

def rmGameToDB(game):
    global cursor
    global webGameDB
    getConnection()
    id = game.DBid
    try:
        cursor.execute(
            "delete from Games where id = {};".format(id)
        )
        print("Successfully remove game to database")
    except mariadb.Error as e:
        print(f"Error remove game to database: {e}")
    webGameDB.commit() 
    webGameDB.close()

class User:
    def __init__(self, Name):
        self.id = None
        self.Name = Name
        self.Path = ""
        self.Carte = ""
        self.Tas = []
        self.Defausse = []

class UserDB(object):
    def __init__(self, Name,id,Path,Carte,Tas,Defausse):
        self.id = id
        self.Name = Name
        self.Path = Path
        self.Carte = Carte
        self.Tas = Tas
        self.Defausse = Defausse

class Game:
    def __init__(self):
        tas = []

        def cartes(n, p):
            couleur = ['C', 'D', 'H', 'S']
            valeur = ['2', '3', '4', '5', '6', '7',
                      '8', '9', '10', 'J', 'Q', 'K', 'A']
            jeu, mains = [], []
            for c in couleur:
                for v in valeur:
                    jeu.append("{}{}".format(v, c))
            for i in range(p):
                main = []
                while len(main) < n:
                    main.append(jeu.pop(randrange(len(jeu))))
                mains.append(main)
            return mains

        nb_cartes, nb_joueurs = 3, 2
        jeux = cartes(nb_cartes, nb_joueurs)
        for i in range(nb_joueurs):

            tas.append(jeux[i])
        tas_p1 = tas[0]
        tas_p2 = tas[1]
        self.nb_manche = 0
        defausse_p1 = []
        defausse_p2 = []

        self.id = randint(0, 1000000)
        self.DBid = None
        self.Players = []
        self.Winner = ""
        self.P1_card = ""
        self.P2_card = ""
        self.P1_tas = tas_p1
        self.P2_tas = tas_p2
        self.P1_defausse = defausse_p1
        self.P2_defausse = defausse_p2
        self.ToursP1 = False
        self.ToursP2 = False
        self.Exaequo = False
        self.Attendre = False
        self.EndGame = False
        self.Info = ""

        GameidList.append(self.id)



class GameDB:
    def __init__(self,id,DBid,Players,Winner,P1_card,P2_card,P1_tas,P2_tas,P1_defausse,P2_defausse,ToursP1,ToursP2,Exaequo,Attendre,EndGame,Info):
        self.id = id
        self.DBid = DBid
        self.Players = Players
        self.Winner = Winner
        self.P1_card = P1_card
        self.P2_card = P2_card
        self.P1_tas = P1_tas
        self.P2_tas = P2_tas
        self.P1_defausse = P1_defausse
        self.P2_defausse = P2_defausse
        self.ToursP1 = ToursP1
        self.ToursP2 = ToursP2
        self.Exaequo = Exaequo
        self.Attendre = Attendre
        self.EndGame = EndGame
        self.Info =Info

    def rmGame(self, player):
        if len(self.Players) == 1:
            rmGameToDB
            GameidList.remove(self.id)
            self.Players.remove(player)
        else:
            self.Players.remove(player)
            print("impossible de supprimer il reste un joueur dans la partie")



def getCurrentUser(name=None):
    for user in UserList:
        if user.Name == name:
            return user


def getCurrentGame(idGame=None):
    for game in GameList:
        if str(game.id) == str(idGame):
            return game



@app.route('/user/<name>/partie/<idPartie>/endGame', methods=['POST', 'GET'])
def endGame(name=None, idPartie=None):
    currentUser = getCurrentUser(name)
    print(currentUser)
    currentGame = getCurrentGame(idPartie)
    print(currentGame)
    winner = "winner"
    nbManche = "nb_manche"
    global GameidList
    global GameList
    global UserList

    if request.method == 'POST':
        if request.form['submit'] == 'Recommencer':
            Game.rmGame(currentGame, currentUser)
            return redirect(url_for('prepartie', name=currentUser.Name))
        if request.form['submit'] == 'Acceuil':
            Game.rmGame(currentGame, currentUser)
            User.rmuser(currentUser)
            return redirect(url_for('login'))
    return render_template('endGame.html', winner=winner, nbManche=nbManche, user=currentUser.Name)


# page de la partie
@app.route('/user/<name>/partie/<idPartie>', methods=['POST', 'GET'])
def partie(name=None, idPartie=None):
    currentUser = getCurrentUser(name)
    print(idPartie)
    currentGame = getCurrentGame(idPartie)
    connectedPlayers = []
    for player in currentGame.Players:
        connectedPlayers.append(player.Name)
    if len(currentGame.Players) == 2:
        currentGame.Players[0].Tas = currentGame.P1_tas
        currentGame.Players[1].Tas = currentGame.P2_tas
        currentGame.Players[0].Carte = currentGame.P1_card
        currentGame.Players[1].Carte = currentGame.P2_card
        currentGame.Players[0].Defausse = currentGame.P1_defausse
        currentGame.Players[1].Defausse = currentGame.P2_defausse
        currentGame.Info = ""

        values = {"2C": 2, "3C": 3, "4C": 4, "5C": 5, "6C": 6, "7C": 7, "8C": 8, "9C": 9, "10C": 10, "JC": 11, "QC": 12, "KC": 13, "AC": 14,
                  "2D": 2, "3D": 3, "4D": 4, "5D": 5, "6D": 6, "7D": 7, "8D": 8, "9D": 9, "10D": 10, "JD": 11, "QD": 12, "KD": 13, "AD": 14,
                  "2H": 2, "3H": 3, "4H": 4, "5H": 5, "6H": 6, "7H": 7, "8H": 8, "9H": 9, "10H": 10, "JH": 11, "QH": 12, "KH": 13, "AH": 14,
                  "2S": 2, "3S": 3, "4S": 4, "5S": 5, "6S": 6, "7S": 7, "8S": 8, "9S": 9, "10S": 10, "JS": 11, "QS": 12, "KS": 13, "AS": 14}

    if request.method == 'POST':

        # si plus ou moins de 2 joueur seul bouton retour fontionne
        if len(currentGame.Players) != 2:
            currentGame.Info = "il faut 2 joueurs pour pouvoir jouer"
            if request.form['submit'] == 'retour':
                Game.rmGame(currentGame, currentUser)
                return redirect(url_for('prepartie', name=currentUser.Name))

        # si deux joueurs:
        if len(currentGame.Players) == 2:
            if request.form['submit'] == 'Découvrir Cartes':

                print("nouveau tours")
                print("---------------------")
                print(currentGame.P1_defausse)
                print(currentGame.P2_defausse)
                print("---------------------")
                print(currentGame.Players[0].Carte)
                print(currentGame.Players[1].Carte)
                print(currentGame.Players[0].Tas)
                print(currentGame.Players[1].Tas)

                if currentGame.ToursP1 & currentGame.ToursP2:
                    currentGame.Attendre = False
                    currentGame.Info = "two player have played"
                    print(currentGame.Players[0].Carte)
                    print(currentGame.Players[1].Carte)

                    # si valeur j1 > j2
                    if values[currentGame.Players[0].Carte] > values[currentGame.Players[1].Carte]:
                        currentGame.Info = "joueur1 gagne la manche"

                        # si il y a une defausse
                        if len(currentGame.Players[0].Defausse) > 0:
                            for carte in currentGame.Players[0].Defausse:
                                currentGame.Players[0].Tas.append(carte)
                            currentGame.Players[0].Defausse = []

                        currentGame.Players[0].Tas.append(
                            currentGame.Players[0].Carte)

                        if len(currentGame.Players[1].Defausse) > 0:
                            for carte in currentGame.Players[1].Defausse:
                                currentGame.Players[0].Tas.append(carte)
                            currentGame.Players[1].Defausse = []

                        currentGame.Players[0].Tas.append(
                            currentGame.Players[1].Carte)

                    # si valeur j1 < j2
                    if values[currentGame.Players[1].Carte] > values[currentGame.Players[0].Carte]:
                        currentGame.Info = "joueur2 gagne"

                        if len(currentGame.Players[1].Defausse) > 0:
                            for carte in currentGame.Players[1].Defausse:
                                currentGame.Players[1].Tas.append(carte)
                            currentGame.Players[1].Defausse = []

                        currentGame.Players[1].Tas.append(
                            currentGame.Players[1].Carte)

                        if len(currentGame.Players[0].Defausse) > 0:
                            for carte in currentGame.Players[0].Defausse:
                                currentGame.Players[1].Tas.append(carte)
                            currentGame.Players[0].Defausse = []

                        currentGame.Players[1].Tas.append(
                            currentGame.Players[0].Carte)

                    # si egalité
                    if values[currentGame.Players[1].Carte] == values[currentGame.Players[0].Carte]:
                        if len(currentGame.Players[0].Tas) < 4 or len(currentGame.Players[1].Tas) < 4:
                            currentGame.Exaequo = True
                        else:
                            currentGame.P1_defausse.append(
                                currentGame.Players[0].Carte)
                            currentGame.P1_defausse.append(
                                currentGame.Players[0].Tas.pop(0))
                            currentGame.P1_defausse.append(
                                currentGame.Players[0].Tas.pop(0))
                            currentGame.P1_defausse.append(
                                currentGame.Players[0].Tas.pop(0))

                            currentGame.P2_defausse.append(
                                currentGame.Players[1].Carte)
                            currentGame.P2_defausse.append(
                                currentGame.Players[1].Tas.pop(0))
                            currentGame.P2_defausse.append(
                                currentGame.Players[1].Tas.pop(0))
                            currentGame.P2_defausse.append(
                                currentGame.Players[1].Tas.pop(0))

                else:
                    if currentGame.ToursP1 == False:
                        currentGame.Info = "le joueur 1 doit jouer"
                        currentGame.Attendre = True
                    if currentGame.ToursP2 == False:
                        currentGame.Info = "le joueur 2 doit jouer"
                        currentGame.Attendre = True

                if len(currentGame.Players[0].Tas) <= 0 or len(currentGame.Players[1].Tas) <= 0 or currentGame.Exaequo:
                    currentGame.Winner = currentGame.Players[1].Name
                    if len(currentGame.Players[1].Tas) == 0:
                        currentGame.Winner = currentGame.Players[0].Name
                    if currentGame.Exaequo:
                        currentGame.Winner = "exaequo !!"
                    currentGame.EndGame = True

                if currentGame.Attendre == False:
                    # actualise les datas
                    currentGame.P1_tas = currentGame.Players[0].Tas
                    currentGame.P2_tas = currentGame.Players[1].Tas
                    currentGame.Players[0].Carte = ""
                    currentGame.Players[1].Carte = ""

                    currentGame.nb_manche += 1
                    currentGame.Players[1].Defausse = []
                    currentGame.Players[0].Defausse = []
                    currentGame.P1_card = currentGame.Players[0].Carte = ""
                    currentGame.P2_card = currentGame.Players[1].Carte = ""
                    currentGame.P1_defausse = currentGame.Players[0].Defausse
                    currentGame.P2_defausse = currentGame.Players[0].Defausse

                    print(currentGame.Players[0].Tas)
                    print(currentGame.Players[1].Tas)
                    currentGame.ToursP1 = False
                    currentGame.ToursP2 = False

            if request.form['submit'] == 'action':

                print(request.path)
                if request.path == "/user/"+currentGame.Players[0].Name+"/partie/"+str(idPartie):
                    if currentGame.ToursP1 == False:
                        currentUser.Carte = currentUser.Tas.pop(0)
                        currentGame.P1_card = currentUser.Carte
                        currentGame.ToursP1 = True
                    else:
                        currentGame.Info = "vous avez deja posé une carte !"
                if request.path == "/user/"+currentGame.Players[1].Name+"/partie/"+str(idPartie):
                    if currentGame.ToursP2 == False:
                        currentGame.Players[1].Carte = currentUser.Tas.pop(0)
                        currentGame.ToursP2 = True
                        currentGame.P2_card = currentUser.Carte
                    else:
                        currentGame.Info = "vous avez deja posé une carte !"

            if request.form['submit'] == 'Abadonner':
                if request.path == "/user/"+currentGame.Players[0].Name+"/partie/"+str(idPartie):
                    currentGame.Winner = currentGame.Players[1].Name
                    currentGame.EndGame = True
                if request.path == "/user/"+currentGame.Players[1].Name+"/partie/"+str(idPartie):
                    currentGame.Winner = currentGame.Players[0].Name
                    currentGame.EndGame = True

    if currentGame.EndGame == True:
        return redirect(url_for("endGame", name=currentUser.Name, idPartie=currentGame.id))

    return render_template('partie.html', name=name, message=currentGame.Info, idPartie=idPartie, connectedPlayer=connectedPlayers, tas=currentUser.Tas, carte=currentUser.Carte, defausse=currentUser.Defausse)

# "lobby" création et chemin vers une partie

@app.route('/user/<name>/', methods=['POST', 'GET'])
def prepartie(name=None):
    currentUser = getCurrentUser(name)
    getUserList()
    print("xxxxxxxxxx")
    print(UserList)
    print("xxxxxxxxxx")
    newLine = ""
    if request.method == 'POST':
        if request.form['submit'] == 'Create Game':
            Game()
            id = GameidList[len(GameidList)-1]
            currentGame = getCurrentGame(id)
            (currentGame.Players).append(currentUser)
            return redirect(url_for("partie", name=currentUser.Name, idPartie=id))
        if request.form['submit'] == 'Join':
            if len(GameidList) > 0:
                id = request.form['comp_select']
                currentGame = getCurrentGame(id)
                (currentGame.Players).append(currentUser)
                return redirect(url_for("partie", name=currentUser.Name, idPartie=id))
            else:
                print("Aucune partie existante")

        if request.form['submit'] == 'Go Back':

            rmUserToDB(currentUser)
            return redirect(url_for("login"))

    return render_template('prepartie.html', name=name, UserList=UserList, GameList=GameList, GameidList=GameidList)


# acceuil et création d'un User
@app.route('/', methods=['POST', 'GET'])
def login():
    validName = True
    Affiche = ""
    if request.method == 'POST':
        newName = request.form.get("name")
        if newName == "":
            validName = False
        for user in UserList:
            if user.Name == newName:
                validName = False
        if validName == True:
            CurrentUser = User(Name=(newName))
            addUserToDB(CurrentUser)

            if request.form['submit'] == 'Play':
                return redirect(url_for('prepartie', name=CurrentUser.Name))
        else:
            Affiche = "Username already used"
    return render_template('index.html', Affiche=Affiche)


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
